<?php

class User {
    public $username;
    public $password;

    /**
     * User constructor.
     * @param $username
     * @param $password
     */
    public function __construct($username = "", $password = "")
    {
        $this->username = $username;
        $this->password = $password;
    }

    public function validate(){
        $errors = [];
        if(strlen($this->username) < 5 ){
            $errors["username"][] = "Слишком короткое имя";
        }
        return $errors;
    }
}

$user = new User("MyName");
$errors = [];
$action = $_REQUEST['action'] ?? '';

if ($action == "save_user"){
    $user->username = $_REQUEST["username"];
    $errors = $user->validate();
}
?>

