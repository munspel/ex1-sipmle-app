<?php include "../src/controllers/main.php" ?>
<!doctype html>
<html lang="en">
<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <!-- Bootstrap CSS -->
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
        integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

  <title>Main page</title>
</head>
<body>
<div class="container">
  <div class="row">
    <div class="col-md-12">
      <a href="/index.php?mod=main">Главная</a><br/>
      <a href="/index.php?mod=about">О нас</a><br/>
      <a href="/index.php?mod=contacts">Контакты</a><br/>

    </div>
    <div class="row">
      <div class="col">
        Username: <?php echo $user->username; ?>
        <form method="post" action='index.php'>
          <div class="form-group">
            <label for="username">Имя пользователя</label>
            <input id="username" type="text" class="form-control" name="username">
              <?php if (key_exists("username", $errors)): ?>
                <small id="emailHelp" class="form-text text-muted"><?php echo $errors["username"][0] ?></small>
              <?php endif; ?>
          </div>
          <input type="hidden" name="mod" value="main">
          <input type="hidden" name="action" value="save_user">
          <div class="form-group">
            <button class="btn btn-primary" type="submit">Сохранить</button>
          </div>
        </form>
      </div>

    </div>
  </div>
  <h1> Главная </h1>
</div>
<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js"
        integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n"
        crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
        integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo"
        crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"
        integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6"
        crossorigin="anonymous"></script>
</body>
</html>
